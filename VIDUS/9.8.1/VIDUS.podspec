#
# Be sure to run `pod lib lint VIDUS.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |spec|
  spec.name             = 'VIDUS'
  spec.version          = '9.8.1'
  spec.summary          = 'This Pod is used to record screen.'
  spec.homepage         = 'https://gitlab.com/frslabs-public/ios/vidus'
  spec.license          = 'MIT'
  spec.author           = { 'Ashish' => 'ashish@frslabs.com' }
  spec.source           =  { :http => 'https://vidus-sdk-ios.repo.frslabs.space/vidus-sdk-ios/9.8.1/Vidus.framework.zip'}
  spec.platform     = :ios
  spec.ios.deployment_target  = '11.0'
  spec.ios.vendored_frameworks = 'VIDUS.framework'
  spec.swift_version = '5.0'
end